
## 2020 本科毕业论文

毕业论文写作指南，请进入 [【Wikis】](https://gitee.com/arlionn/bech19/wikis/Home) 查看。



## $\color{red}{News}$

> **讨论安排：**
- **时间地点：** `2020.10.28 下午 4:30`，岭南堂 4 楼咖啡厅
- **讨论内容：** 文献研读。你们先看看 JF，JFE，RFS，AER 上的最近两年的论文，也找找这些大牛最近的 Working paper，看看有哪些感兴趣的主题，每个人汇报 3-4 篇论文的大体思路，研究内容和主要贡献。 
- **资料提交：** 请于 2020.10.28 日下午 4:10 前将你的读书笔记放在自己的【坚果云共享文件夹/读书报告】子文件夹中

> 加入平台：&#x1F34E; 
1. 申请一个码云账号 ( <https://gitee.com> );
2. 点击 [-**邀请码**-](https://gitee.com/arlionn/bech/invite_link?invite=99d36473e26408ba4d9f9b986084c92bff6d961e56bd9f95b7993663152babc53bb088706166be5a6121c5559972f7e3)，获取编辑权限，日后可以在自己的文件夹中存储一些论文相关的资料和 Stata 代码等。
3. 点击右上角的【**Fork**】按钮，可以把这个项目 Fork 到你自己的主页下，定期同步，以便查看。亦可在你的主页下新开一些【仓库】，用于存储你的资料。 

&emsp;

## 写作指南
**1. 基本要求**   
请认真阅读 [【Wikis】](https://gitee.com/arlionn/bech/wikis/Home)，里面详述了论文各个阶段的要求和注意事项，也提供了一些参考资料。

**2.坚果云贡献文件夹**  
日后论文写作过程中各个版本的文件都统一放在你的【坚果云共享文件夹】下，以便我及时查看给出修改意见，各个版本按照【v1_姓名_论文标题简称_2020.10.27.docx】格式存放。

**3. 文件夹设置说明**  
- 【读书笔记】：存放选题过程中研读经典文献产生的读书笔记。重点记录作者核心观点、研究方法和模型、数据来源、研究结论，以及 **你认为值得借鉴或可以扩展** 的地方 (这是你选题的主要切入点)。
- 【参考文献】：论文写作过程中研读的文献，命令规则：**作者-年份-期刊简称-论文标题.pdf**。
- 【论文】：存放各个阶段的论文文档，如开题报告、论文初稿等。各个版本的命令规则为「v1_姓名_论文标题简称_2020.10.27.docx」。
- 【姓名_Emp】：存放论文实证分析过程中的数据、dofiles 等文档。以便于我执行这个文件中的 dofiles 后可以重现你论文中的所有实证结果。参见：
  - [论文重现网站大全](https://www.lianxh.cn/news/e87e5976686d5.html) 
  - [可重复性研究：如何保证你的研究结果可重现？](https://www.lianxh.cn/news/6d3f9bbbdef36.html)
  - [我的甲壳虫：经典论文重现](https://gitee.com/arlionn/paper101) 

**4. Stata 文件夹：论文可重现要求**   
写论文过程中请整理好你的 Stata 相关文件夹，论文写作过程中的所有 dofile 我会定期执行，以便结果可以重现。文件夹名称为：【**姓名_Emp**】，参见 [连老师-dofile 范本](https://gitee.com/arlionn/bech/wikis/Sample_Stata_dofile.do.md?sort_id=3034687)。

```stata
*-定义论文写作文件夹 (请将如下命令贴入你的 Stata dofile 中，一次性执行)

  global path `"xxxx"'  //定义文件路径
  global D    "$path\data"      //范例数据
  global R    "$path\refs"      //参考文献
  global Out  "$path\out"       //结果：图形和表格
  adopath +   "$path\adofiles"  //自编程序 
  cd "$D"
  set scheme s2color  
```
最终的文件结构如下：
```
D:\连玉君_Emp/.
│  _Main_Lian.do
│  
├─adofiles
├─data
├─out
└─refs
```

## 资源
大家写作时可以参考的书籍和文献：
- **连享会图书馆：** [计量教材](https://quqi.gblhgk.com/s/880197/wqZT9wv6IKpd4Wh1)  || [Stata Journal](https://quqi.gblhgk.com/s/880197/Uv7IUeASWJnvyjUd) || [SJ单篇PDF](https://www.lianxh.cn/news/12ffe67d8d8fb.html) 
- **常用工具和链接：** [连玉君的链接](https://www.lianxh.cn/news/9e917d856a654.html) || [五分钟 Markdown](https://gitee.com/arlionn/md) 
- **学术资源：**
  - 搜索引擎：[百度学术](http://xueshu.baidu.com/) | [微软学术](https://academic.microsoft.com/home) | [iData论文下载](https://www.cn-ki.net/)
  - 论文重现: [论文重现网站大全](https://www.lianxh.cn/news/e87e5976686d5.html) | [Harvard dataverse](https://dataverse.harvard.edu/dataverse)  | [JFE](http://jfe.rochester.edu/data.htm)  | [github](https://github.com/search?utf8=%E2%9C%93&q=stata&type=) | [论文可重现要求](https://www.lianxh.cn/news/6d3f9bbbdef36.html)